---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(assertthat)
library(ggplot2)
library(tidyverse)

message("We will focus on Cinnamon squirrels")

primary_fur_color <- "Cinnamon"
message(glue("We will focus on {primary_fur_color} squirrels"))

```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Get a message with a fur color

You can get a message with the fur color of interest with `get_message_fur_color()`.
  
```{r function-get_message_fur_color}
#' Get a message with the fur color of interest
#'
#' @param primary_fur_color character. The primary fur color of interest
#' @importFrom glue glue
#' @return a message . Used for side effect. Outputs a message in the console
#' @export
#'
#' @examples
get_message_fur_color <- function(primary_fur_color){
  
    message(glue("We will focus on {primary_fur_color} squirrels"))
  
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")

get_message_fur_color(primary_fur_color = "Black")

```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {

  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon")
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"), 
    regexp = "We will focus on Black squirrels"
  )
  
})
```
 
 
# study_activity
    
```{r function-study_activity}
#' study_activity
#' 
#' study activity
#' 
#' @param df_squirrels_act data.frame le jeu de donnees sur mes ecureuils
#' @param col_primary_fur_color character une couleur
#' @importFrom assertthat assert_that
#' @importFrom dplyr filter
#' @importFrom ggplot2 ggplot aes geom_col labs scale_fill_manual
#' @importFrom glue glue
#' @return
#' 
#' @export
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  check_squirrel_data_integrity(df_squirrels_act)
  
  assert_that(is.data.frame(df_squirrels_act))
  assert_that(is.character(col_primary_fur_color))
  
  table <- df_squirrels_act %>% 
    filter(primary_fur_color == col_primary_fur_color)
    
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}
```
  
```{r example-study_activity}

data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
```
  
```{r tests-study_activity}
test_that("study_activity works", {
  expect_true(inherits(study_activity, "function")) 
  
  library(dplyr)
  
  data(data_act_squirrels)
  resultat <- study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
  
  expect_equal(length(resultat), 2)
  
  expect_s3_class(resultat[[2]], "ggplot")
  
  expect_s3_class(resultat[[1]], "data.frame")
  
  expect_error(study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = 500))
  
  expect_error(study_activity(df_squirrels_act = iris, 
               col_primary_fur_color = "Gray"))
    
  expect_error(study_activity(
      df_squirrels_act = data_act_squirrels %>% select(-primary_fur_color), 
               col_primary_fur_color = "Gray"))
    
})
```
 
# Save an output as a csv file

You can save an output as a csv file with `save_as_csv()`. This function can be used, for instance, to save the filtered table created by `study_activity()`.
    
```{r function-save_as_csv}
#' Save an output as a csv file
#'
#' @param data Data frame. The table to be saved as a csv file.
#' @param path Character. Path where the csv file shloud be saved.
#' @param ... Additional arguments of \code{utils::\link{write.csv2}()}
#' 
#' @importFrom assertthat assert_that not_empty has_extension is.dir is.writeable
#' @importFrom utils write.csv2
#' 
#' @return The path to the created csv file.
#' @export
#'
#' @examples
save_as_csv <- function(data, path, ...){
  
  assert_that(is.data.frame(data))
  assert_that(not_empty(data))
  assert_that(has_extension(path, ext = "csv"))
  assert_that(is.dir(dirname(path)))
  assert_that(is.writeable(dirname(path)))
  
  write.csv2(x = data, file = path, ...)
  
  return(invisible(path))
}
```

```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  # Everything is ok
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)

  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")), 
              regexp = NA)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")) %>% browseURL(), 
               regexp = NA)
  
  unlink(my_temp_folder, recursive = TRUE)
  
  # Error
  expect_error(iris %>% save_as_csv("output.xlsx"), 
               regexp = "does not have extension csv")
  
  expect_error(NULL %>% save_as_csv("output.csv"),
               regexp = "data is not a data frame")
  
  expect_error(iris %>% save_as_csv("does/no/exist.csv"), 
               regexp = "does not exist")
  
  expect_error(iris %>% save_as_csv("/usr/bin/output.csv"), 
               regexp = "not writeable")
  
})
```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "Study the squirrels")
```
