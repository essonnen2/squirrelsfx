% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/save_as_csv.R
\name{save_as_csv}
\alias{save_as_csv}
\title{Save an output as a csv file}
\usage{
save_as_csv(data, path, ...)
}
\arguments{
\item{data}{Data frame. The table to be saved as a csv file.}

\item{path}{Character. Path where the csv file shloud be saved.}

\item{...}{Additional arguments of \code{utils::\link{write.csv2}()}}
}
\value{
The path to the created csv file.
}
\description{
Save an output as a csv file
}
\examples{
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
}
